package com.gitlab.soshibby.issuereporter.messageappenders.grouping;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.messageappender.MessageAppender;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginDefinition;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GroupingAppender implements MessageAppender {

    private static final Logger log = LoggerFactory.getLogger(GroupingAppender.class);
    private Config configuration;
    private PluginResolver pluginResolver;
    private List<MessageAppender> childAppenders;

    public GroupingAppender(PluginResolver pluginResolver) {
        this.pluginResolver = pluginResolver;
    }

    @Override
    public void init(String config) {
        configuration = Config.from(config);
        validateConfiguration(configuration);

        childAppenders = getChildMessageAppenders(configuration.getAppenders());
    }

    @Override
    public String createMessageFrom(LogStatement logTrigger, List<LogStatement> logStatements) {
        StringBuilder sb = new StringBuilder();

        for (MessageAppender childMessageAppender : childAppenders) {
            sb.append(childMessageAppender.createMessageFrom(logTrigger, logStatements));
        }

        if (sb.length() != 0) {
            sb.append('\n');
            sb.append('\n');
        }

        return sb.toString();
    }

    private void validateConfiguration(Config config) {
        Assert.notNull(config, "Configuration is null.");
        Assert.notEmpty(config.getAppenders(), "Appenders is null or empty in config.");
    }

    private List<MessageAppender> getChildMessageAppenders(List<PluginDefinition> plugins) {
        return pluginResolver.resolve(plugins, MessageAppender.class);
    }
}

