package com.gitlab.soshibby.issuereporter.messageappenders.grouping;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.soshibby.issuereporter.pluginresolver.PluginDefinition;

import java.io.IOException;
import java.util.List;

@JsonIgnoreProperties("class")
public class Config {
    private static ObjectMapper mapper = new ObjectMapper();
    private List<PluginDefinition> appenders;

    public List<PluginDefinition> getAppenders() {
        return appenders;
    }

    public void setAppenders(List<PluginDefinition> appenders) {
        this.appenders = appenders;
    }

    public static Config from(String config) {
        try {
            return mapper.readValue(config, Config.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String toJson() {
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

